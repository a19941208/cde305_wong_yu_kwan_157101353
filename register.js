console.log('register.js is connected to the HTML file successfully.')

//Hidden the success message box
var successBox = document.getElementById("regSuccess");
successBox.style.visibility = "hidden";

//Use for vaildation, if the input is vailded, it will turn to true
var userNameVaild = false;
var userNameDup = false;
var nameVaild = false;
var emailVaild = false;
var pwVaild = false;
var ageVaild = false;

//get the firebase connection
var fireBaseRef = new Firebase("https://popping-fire-1938.firebaseio.com/");

//Show the list of registered user
registeredUser();


//This function used to vaildate the user input
document.getElementById("vaildation").onclick = function(){
	
	//Get the username, name, email, password and age value from the text field
	var username = document.getElementById("username").value; //
	var name = document.getElementById("name").value;
	var email = document.getElementById("email").value;
	var password = document.getElementById("password").value;
	var age = document.getElementById("age").value;

	//Call the checking methods
	checkuserName(username);
	checkNameContainNumber(name);
	checkEmail(email);
	checkPassword(password);
	checkAge(age);
	
	//When all fields were filled in correctly, start to upload the information to firebase
	if(userNameVaild == true && nameVaild == true && emailVaild == true && pwVaild && ageVaild == true){
		
		//Call firebase method to upload information
		fireBaseRef.on("child_added", function(snapshot,prevChildKey){
			var currentData = snapshot.val();
			var existUserName = currentData.username;
			
			//Check either the user name is duplicated
			if(username == existUserName){
				document.getElementById("usernameErr").innerHTML = "Duplicated user name";
				userNameDup = true;
			}
		});
		
		//Push an object to firebase
		if(userNameDup == false){
			fireBaseRef.push({
			 	username: username,
			 	name: name,
			 	email: email,
			 	password: password,
			 	age: age
		 	});
		 	
		 //Hide the description element
		 document.getElementById("des").style.visibility ="hidden";
		 //Show the success message box
		 showSuccessBox(username, name, email, password, age);
		}
	}
}

//Check user name
function checkuserName(username){
	//Check user name is less than 3 charator or number or empty
	if(username.length == 0 || username.length < 3){
		//Show error message
		document.getElementById("usernameErr").innerHTML = "Please enter a vaild user name";
	} else {
		//Turn to true
		userNameVaild = true;
	}
}

//Check name
function checkNameContainNumber(fieldText){
	//Create Regular expression which is only contain A-Z (a-z) and blank space
	var matches = /^[a-zA-Z\s]*$/;
		
		//Check either the name is vaild
		if(matches.test(fieldText) && fieldText != ""){
			//Turn to true
			nameVaild = true;
		} else {
			//Show error message
			document.getElementById("nameErr").innerHTML = "Please enter a vaild name";
		}
}

//Check e-mail
function checkEmail(email){
	//Create regular expression for e-mail format
	var matches = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
		//Check either the e-mail is vaild
		if(matches.test(email) && email != ""){
			//Turn to true
			emailVaild = true;
		} else {
			//Show error message
			document.getElementById("emailErr").innerHTML = "Please enter a vaild email address";
		}
}

//Check password
function checkPassword(pw){
	//Check password length is more than 8
	if(pw.length < 8){
		//Show error message
		document.getElementById("passwordErr").innerHTML = "Password must be more than 8 charator";
	} else{
		//Turn to true
		pwVaild = true;
	}
}

//Check age
function checkAge(age){
	
	//Get the element
	var ageErrfield = document.getElementById("ageErr"); 
	//Create regular expression 
	var matches = /^[A-Za-z]+$/;
	var intAge = parseInt(age);
		
		//Check either age is vaild
		if(matches.test(intAge)){
			//Show error message
			ageErrfield.innerHTML = "Age should be number";
		} else if(age == 0 || age > 120){
			//Show error message
			ageErrfield.innerHTML = "Are you human";
		} else {
			//Turn to true
			ageVaild = true;
		}
}

//Show the success message
function showSuccessBox(username, name, email, password, age){
	var regBox = document.getElementById("regBox");
		
		//Hide the registeration box
		regBox.style.visibility = "hidden";
		regBox.style.height = "0";
		successBox.style.visibility = "visible";
		document.getElementById("message").innerHTML = "You have register successfully.";
		document.getElementById("showUserName").innerHTML = username;
		document.getElementById("showName").innerHTML = name;
		document.getElementById("showEmail").innerHTML = email;
		document.getElementById("showPw").innerHTML = password;
		document.getElementById("showAge").innerHTML = age;
		//Update registered user list
		registeredUser();
}

//Reload the page
document.getElementById("reset").onclick = function(){
	window.reload();
}

//Calculate the average user age
document.getElementById("calculate").onclick = function(){
	var total = 0
	var totalAge = 0;
	
	//Get user age from firebase
	fireBaseRef.on("child_added", function(snapshot,prevChildKey) {
  	var currentData = snapshot.val();
	var existAge = currentData.age;
	total++;
	totalAge += parseInt(existAge); 
	});
	
	//Calculate the average age
	var Average = totalAge / total;
	//Show the calculated age to alert prompt
	alert("Average user age: " + Average);
}

//Show registered user list
function registeredUser(){
	//Get the userList element
	var userList = document.getElementById("userList");
	//Set it empty
	userList.innerHTML = "";
	//Creae new element to show the bar
	var userBar = document.createElement("h3");
	userBar.setAttribute("id", "userBar");
	var t = document.createTextNode("Registered User");
	
	userBar.appendChild(t);
	userList.appendChild(userBar);
	
	//Get information from firebase
	fireBaseRef.on("child_added", function(snapshot,prevChildKey){
		var currentData = snapshot.val();
		var existUserName = currentData.username;
		var li = document.createElement("li");
		li.setAttribute("id", "ori");
		var t = document.createTextNode(existUserName);
		
			//display registered users name
			li.appendChild(t);
			userList.appendChild(li);
	});
}

//Sort the list by name or age
document.getElementById("sorting").onclick = function(){
	var selectList = document.getElementById("sort");
	var selectedSortVlaue = selectList.options[selectList.selectedIndex].text;
	var userList = document.getElementById("userList");
	var userBar = document.createElement("h3");
	
	
	if(selectedSortVlaue == "Age"){
		//Empty the list
		userList.innerHTML = "";
		userBar.appendChild(document.createTextNode("Registered User(Sort By Age)"));
		userList.appendChild(userBar);
		
		//Get information from firebase
		fireBaseRef.orderByChild("age").on("child_added", function(snapshot,prevChildKey){
			var currentData = snapshot.val();
			var existUserName = currentData.username;
			var li = document.createElement("li");
			var t = document.createTextNode(existUserName);
			
				//Display the information
				li.appendChild(t);
				userList.appendChild(li);
		});
	} else{
		//Empty the list
		userList.innerHTML = "";
		userBar.appendChild(document.createTextNode("Registered User(Sort By Name)"));
		userList.appendChild(userBar);
		
		//Get information from firebase
		fireBaseRef.orderByChild("username").on("child_added", function(snapshot,prevChildKey){
			var currentData = snapshot.val();
			var existUserName = currentData.username;
			var li = document.createElement("li");
			var t = document.createTextNode(existUserName);
				
				//Display the information
				li.appendChild(t);
				userList.appendChild(li);
		});
	}
}

//Filter the age
document.getElementById("filterAge").onclick = function() {
	var selectList = document.getElementById("ageSort");
	var selectedSortVlaue = selectList.options[selectList.selectedIndex].text;
	var userList = document.getElementById("userList");
	var userBar = document.createElement("h3");
	var age = document.getElementById("ageNumber").value;
	
	if(selectedSortVlaue == ">="){
		userList.innerHTML = "";
		userBar.appendChild(document.createTextNode("Registered User(Which is " + selectedSortVlaue + age + ")"));
		userList.appendChild(userBar);
		
		fireBaseRef.on("child_added", function(snapshot,prevChildKey){
			var currentData = snapshot.val();
			var existAge = currentData.age;
			var existUserName = currentData.username;
			var li = document.createElement("li");
			var t = document.createTextNode(existUserName);
			
			if(parseInt(existAge) >= parseInt(age)){
				li.appendChild(t);
				userList.appendChild(li);
			}
		});
	} else{
		userList.innerHTML = "";
		userBar.appendChild(document.createTextNode("Registered User(Which is " + selectedSortVlaue + age + ")"));
		userList.appendChild(userBar);
		
		fireBaseRef.on("child_added", function(snapshot,prevChildKey){
			var currentData = snapshot.val();
			var existAge = currentData.age;
			var existUserName = currentData.username;
			var li = document.createElement("li");
			var t = document.createTextNode(existUserName);
			
			if(parseInt(existAge) <= parseInt(age)){
				li.appendChild(t);
				userList.appendChild(li);
			}
		});
	}
}

